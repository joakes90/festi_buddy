//
//  AboutViewController.swift
//  Festi Buddy
//
//  Created by Justin Oakes on 2/26/15.
//  Copyright (c) 2015 Oklasoft. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {

    var closed: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
