//
//  FestivalController.swift
//  Festi Buddy
//
//  Created by Justin Oakes on 8/16/15.
//  Copyright (c) 2015 Oklasoft. All rights reserved.
//

import UIKit

class FestivalController: NSObject {
    
    static let sharedInstance = FestivalController()
    
    var festivals: [Festivals]
    
    let delegate: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    
    override init() {
        self.festivals = []
        super.init()
    }
    
    func updateFestivals(rawArray: [[String:AnyObject]]) {
        
        let dateFormater: NSDateFormatter = NSDateFormatter()
        dateFormater.dateFormat = "yyyy-MM-dd"
        
        let fetchRequest: NSFetchRequest = NSFetchRequest(entityName: "Festival")
        let customFests: [Festival] = try! delegate.managedObjectContext.executeFetchRequest(fetchRequest) as! [Festival]
        
        for fest: Festival in customFests {
            // converting date to compatable string
            let dateFormater: NSDateFormatter = NSDateFormatter()
            dateFormater.dateFormat = "yyyy-MM-dd"
            let dateString: String = dateFormater.stringFromDate(fest.date)
            
            let convertedFest: Festivals = Festivals(title: fest.title, date: dateString, lat: fest.latitude, long: fest.longitude)
            
            self.festivals.append(convertedFest)
        }
        if rawArray.count > 0 {
            for index in rawArray {
                let newFestivalObject: Festivals = Festivals(dict: index)
                self.festivals.append(newFestivalObject)
            }
            NSNotificationCenter.defaultCenter().postNotification(NSNotification(name: Constants.kFestivalsRetrievednotification, object: nil, userInfo: nil))
        }
    }
    
    func getFestivalsFromJSON() {
        self.festivals = []
        let urlRequest: NSURLRequest = NSURLRequest(URL: NSURL(string: Constants.kFestivalsURL)!)
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithRequest(urlRequest) { (data, responce, error) in
            
            guard error == nil else {
                print("Error: \(error)")
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    NSNotificationCenter.defaultCenter().postNotificationName(Constants.kNoInternetnotification, object: nil)
                })
                return
            }
            
            guard let data = data else {
                fatalError("error was nil, but no data. This should never happen")
            }
            
            guard let responce = NSString(data: data, encoding: NSUTF8StringEncoding) else {
                print("Couldn't decode data")
                return
            }
            
            do {
                let rawArray: [[String:AnyObject]] = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableLeaves) as! [[String:AnyObject]]
                let urls: [NSURL] = NSFileManager.defaultManager().URLsForDirectory(NSSearchPathDirectory.DocumentDirectory, inDomains: NSSearchPathDomainMask.UserDomainMask)
                var url: NSURL = urls[0]
                url = url.URLByAppendingPathComponent("backup.json")
                data.writeToURL(url, atomically: true)
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    FestivalController.sharedInstance.updateFestivals(rawArray)
                })
                
            } catch {
                let urls: [NSURL] = NSFileManager.defaultManager().URLsForDirectory(NSSearchPathDirectory.DocumentDirectory, inDomains: NSSearchPathDomainMask.UserDomainMask)
                var url: NSURL = urls[0]
                url = url.URLByAppendingPathComponent("backup.json")

                if NSFileManager.defaultManager().fileExistsAtPath(url.path!) {
                    let jsonData: NSData = NSData(contentsOfURL: url)!
                    do {
                        let rawArray: [[String:AnyObject]] = try NSJSONSerialization.JSONObjectWithData(jsonData, options: NSJSONReadingOptions.MutableLeaves) as! [[String:AnyObject]]
                        
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            FestivalController.sharedInstance.updateFestivals(rawArray)
                        })
                        
                    } catch {
                        print("local json data corrupted")
                    }
                }
                print("failed to get data, using cached data \n \(error)")
                print(responce)
            }
            
        }
        
        task.resume()
    }
    
    func removeFestvalAtIndex(index: Int) {
        let fetchRequest: NSFetchRequest = NSFetchRequest(entityName: "Festival")
        let customFests: [Festival] = try! delegate.managedObjectContext.executeFetchRequest(fetchRequest) as! [Festival]
        let itemToBeRemoved = customFests[index]
        delegate.managedObjectContext.deleteObject(itemToBeRemoved)
        do {
            try delegate.managedObjectContext.save()
            self.getFestivalsFromJSON()
        } catch {
            print("deletion of festival failed")
        }
    }
   
}
