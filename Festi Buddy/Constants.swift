//
//  Constants.swift
//  Festi Buddy
//
//  Created by Justin Oakes on 3/6/16.
//  Copyright © 2016 Oklasoft. All rights reserved.
//


struct Constants {
    static let kFestivalsURL: String = "https://oklasoftware.com/festibuddy/festsPlist.json"
    static let kFestivalsRetrievednotification = "festivalsRetreaved"
    static let kNoInternetnotification = "noInternet"
    static let kNoConnectionMessage = "It looks like you currently do not have access to the internet. The next time you're connected I'll cache festival info so you don't see me again"
}
