//
//  Festivals.swift
//  Festi Countdown
//
//  Created by Justin Oakes on 1/4/15.
//  Copyright (c) 2015 Justin Oakes. All rights reserved.
//

import Foundation
import CoreLocation

class Festivals {
    let date: NSDate?
    let title: NSString
    let detailImageString: NSString?
    var tableImage: String
    let gregorianCalendar = NSCalendar(identifier: NSCalendarIdentifierGregorian)
    let customFest: Bool
    var days: Int = 0
    var hours: Int = 0
    var minutes: Int = 0
    
    var dayString: String = ""
    var hourString: String = ""
    var minuteString: String = ""
    
    var lat: NSNumber = 0
    var long: NSNumber = 0
    var location: String = ""
    var lineup: [String] = []
    
    
    
    
    
    func timeTillFest(date: NSDate)->NSDateComponents  {
        let timeZone = NSTimeZone.localTimeZone()
        let interval = timeZone.secondsFromGMT
        let currentDate = NSDate().dateByAddingTimeInterval(NSTimeInterval(interval))
        let toDate = date.dateByAddingTimeInterval(NSTimeInterval(interval))
        let componets: NSCalendarUnit = [NSCalendarUnit.Day, NSCalendarUnit.Hour, NSCalendarUnit.Minute]
        
        let currentComponets = gregorianCalendar?.components(componets, fromDate: currentDate, toDate: toDate, options: [])
        
        return currentComponets!
    }
    
   
    
    
    
    func update(){
        let componets = timeTillFest(self.date!)
        
        self.days = componets.day
        self.hours = componets.hour
        self.minutes = componets.minute
        
        if Int(self.days) <= 0 && Int(self.hours) <= 0 && Int(self.minutes) <= 0 {
            self.dayString = "0"
            self.hourString = "0"
            self.minuteString = "0"
            
        } else {
            self.dayString = String(self.days)
            self.hourString = String(self.hours)
            self.minuteString = String(self.minutes)
        }

    }
    
    init(dict: [String:AnyObject]?){
        
        let dateFormater: NSDateFormatter = NSDateFormatter()
        dateFormater.dateFormat = "yyyy-MM-dd"
        
        self.title = dict!["title"] as! String
        
        let date = dateFormater.dateFromString(dict!["date"] as! String)
        self.date = date
        self.tableImage = dict!["tableImageString"] as! String
        self.customFest = false
        self.detailImageString = nil
        self.dayString = String(self.days)
        self.hourString = String(self.hours)
        self.minuteString = String(self.minutes)
        self.location = dict!["location"] as! String
        self.lat = dict!["lat"] as! NSNumber
        self.long = dict!["long"] as! NSNumber
        self.lineup = dict!["lineup"] as! [String]
        
    }
    
    init(title: NSString, date: NSString?, lat: NSNumber, long: NSNumber){
        let dateFormater: NSDateFormatter = NSDateFormatter()
        dateFormater.dateFormat = "yyyy-MM-dd"
        
        self.title = title
        self.date = dateFormater.dateFromString(date as! String)
        self.lat = lat
        self.long = long
        self.customFest = true
        
        self.tableImage = "defaultTable.png"
        self.detailImageString = nil
        self.lineup = []
        
    }
    
    func stringForLocation(lat: NSNumber, long: NSNumber) {
        let geoCoder: CLGeocoder = CLGeocoder()
        geoCoder.reverseGeocodeLocation(CLLocation(latitude: CLLocationDegrees(lat.doubleValue), longitude: CLLocationDegrees(long.doubleValue))) { (placeMarks, error) -> Void in
            if error != nil {
                print(error)
            } else if (placeMarks != nil){
                let placeMark: CLPlacemark = placeMarks![0]
                self.location = "\(placeMark.locality!), \(placeMark.administrativeArea!)"
                NSNotificationCenter.defaultCenter().postNotificationName(Constants.kFestivalsRetrievednotification, object: nil)
            }
        }
    }
}

