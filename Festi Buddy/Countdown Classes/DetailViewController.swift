//
//  DetailViewController.swift
//  Festi Buddy
//
//  Created by Justin Oakes on 4/10/16.
//  Copyright © 2016 Oklasoft. All rights reserved.
//

import UIKit
import MapKit

class DetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var tableView: UITableView!
    
    var fest: Festivals?
    
    override func viewDidAppear(animated: Bool) {
        let tap = UITapGestureRecognizer(target: self, action: #selector(DetailViewController.openNavigation))
        tap.numberOfTapsRequired = 3
        self.mapView.addGestureRecognizer(tap)
        self.navigationController?.navigationBar.topItem?.title = fest?.title as? String
        let coordinate: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: CLLocationDegrees(self.fest!.lat.doubleValue), longitude: CLLocationDegrees(self.fest!.long.doubleValue))
        let camera: MKMapCamera = MKMapCamera(lookingAtCenterCoordinate: coordinate, fromDistance: CLLocationDistance(5000.0), pitch: 0.0, heading: 0.0)
        let annotation: MKPointAnnotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        annotation.title = self.fest?.title as? String
        annotation.subtitle = "\(self.fest!.dayString) Days \(self.fest!.hourString) Hours and \(self.fest!.minuteString) Minutes"
        self.mapView.addAnnotation(annotation)
        self.mapView.setCamera(camera, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

func openNavigation() {
    let placeMark: MKPlacemark = MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: CLLocationDegrees(self.fest!.lat.doubleValue), longitude: CLLocationDegrees(self.fest!.long.doubleValue)), addressDictionary: nil)
    let destination: MKMapItem = MKMapItem(placemark: placeMark)
    destination.openInMapsWithLaunchOptions([MKLaunchOptionsDirectionsModeKey:MKLaunchOptionsDirectionsModeDriving])
}

    // MARK: - Table View Delegate methods
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.fest?.lineup.count)! > 0 ? (self.fest?.lineup.count)! : 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCellWithIdentifier("cell")!
        if self.fest?.lineup.count > 0 {
            cell.textLabel?.text = fest?.lineup[indexPath.row]
        } else {
            cell.textLabel?.text = "No Lineup Yet"
        }
        cell.textLabel?.font = UIFont(name: "RageItalic", size: 24.0)
        cell.textLabel?.textColor = UIColor(red: 421.0/255.0, green: 229.0/255.0, blue: 16.0/255.0, alpha: 1)
        return cell
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var term: String = (self.tableView.cellForRowAtIndexPath(indexPath)?.textLabel?.text)!
        term = term.stringByReplacingOccurrencesOfString(" ", withString: "+")
        let url: NSURL = NSURL(string: "http://www.youtube.com/results?search_query=\(term)")!
        if term != "No+Lineup+Yet" && UIApplication.sharedApplication().canOpenURL(url) {
            UIApplication.sharedApplication().openURL(url)
        }
    }
}
