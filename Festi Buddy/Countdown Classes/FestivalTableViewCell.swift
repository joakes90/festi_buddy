//
//  FestivalTableViewCell.swift
//  Festi Buddy
//
//  Created by Justin Oakes on 3/5/16.
//  Copyright © 2016 Oklasoft. All rights reserved.
//

import UIKit

class FestivalTableViewCell: UITableViewCell {

    @IBOutlet var backgroundImage: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var locationLabel: UILabel!
    @IBOutlet var timeLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
