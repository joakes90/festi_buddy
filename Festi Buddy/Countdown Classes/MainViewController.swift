//
//  MainViewController.swift
//  Festi Buddy
//
//  Created by Justin Oakes on 3/5/16.
//  Copyright © 2016 Oklasoft. All rights reserved.
//

import UIKit

class MainViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var timer: NSTimer = NSTimer()
    var failedConnectingToNetwork: Bool?
    private var token: dispatch_once_t = 0
    
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MainViewController.updateListedInfo), name: Constants.kFestivalsRetrievednotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MainViewController.alertNoInternet), name: Constants.kNoInternetnotification, object: nil)
       self.timer = NSTimer.scheduledTimerWithTimeInterval(30.0, target: self, selector: #selector(MainViewController.updateListedInfo), userInfo: nil, repeats: true)
        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(animated: Bool) {
        FestivalController.sharedInstance.getFestivalsFromJSON()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateListedInfo() {
        self.tableView.reloadData()
    }
    
    func alertNoInternet() {
        dispatch_once(&token, { () -> Void in
            let alert: UIAlertController = UIAlertController(title: "Uh oh",
                message: Constants.kNoConnectionMessage,
                preferredStyle: UIAlertControllerStyle.Alert)
            let okAction: UIAlertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil)
            alert.addAction(okAction)
            self.presentViewController(alert, animated: true, completion: nil)
        })

    }
    //MARK: - TableView Delegate Methods
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if FestivalController.sharedInstance.festivals.count > 0 {
            return FestivalController.sharedInstance.festivals.count
        } else {
            return 0
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: FestivalTableViewCell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! FestivalTableViewCell
        let fest: Festivals = FestivalController.sharedInstance.festivals[indexPath.row]
        fest.update()
        cell.timeLabel.text = "\(fest.dayString) \(fest.days == 1 ? "Day" : "Days") \(fest.hourString ) \(fest.hours == 1 ? "Hour " : "Hours ") \(fest.minuteString) \(fest.minutes == 1 ? "Minute" : "Minutes")"
        cell.nameLabel.text = fest.title as String
        cell.locationLabel.text = fest.location == "" ? "" : fest.location
        if fest.tableImage != "" && !fest.tableImage.containsString("http") {
            cell.backgroundImage.image = UIImage(named: fest.tableImage)
        } else {
            cell.backgroundImage.image = UIImage(named: "cellbackground")
        }
        if fest.title.length > 24 {
            cell.nameLabel.font = UIFont(name: "RageItalic", size: 20.0)
        }

        return cell
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        if FestivalController.sharedInstance.festivals[indexPath.row].customFest {
            return true
        } else {
            return false
        }
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == UITableViewCellEditingStyle.Delete {
            FestivalController.sharedInstance.removeFestvalAtIndex(indexPath.row)
        }
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "detail" {
            let detailVC: DetailViewController = segue.destinationViewController as! DetailViewController
            detailVC.fest = FestivalController.sharedInstance.festivals[(self.tableView.indexPathForSelectedRow?.row)!]
        }
    }
    

}
